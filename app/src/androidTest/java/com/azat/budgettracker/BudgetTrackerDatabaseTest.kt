package com.azat.budgettracker

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.azat.budgettracker.data.persistent.BudgetTrackerDatabase
import com.azat.budgettracker.data.persistent.TransactionDao
import com.google.common.truth.Truth.assertThat
import com.azat.budgettracker.domain.entities.ExpenseType
import com.azat.budgettracker.domain.entities.Transaction
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class BudgetTrackerDatabaseTest {

    private lateinit var database: BudgetTrackerDatabase
    private lateinit var transactionDao: TransactionDao

    @Before
    fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(context, BudgetTrackerDatabase::class.java).build()
        transactionDao = database.transactionDao()
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun testInsertTransactionShouldAddToDatabase() {
        val transaction = Transaction(
            id = 1,
            amount = 40.0,
            date = Date(),
            description = "something to eat",
            expenseType = ExpenseType.OFFICE_EXPENSES
        )
        transactionDao.insert(transaction)
        val allTransactions = transactionDao.getAll()
        assertThat(allTransactions).contains(transaction)
    }

    @Test
    fun testDeleteTransactionShouldDeleteFromDatabase() {
        val transaction = Transaction(
            id = 1,
            amount = 40.0,
            date = Date(),
            description = "something to eat",
            expenseType = ExpenseType.OFFICE_EXPENSES
        )
        transactionDao.insert(transaction)
        var allTransactions = transactionDao.getAll()
        assertThat(allTransactions).contains(transaction)
        transactionDao.delete(transaction)
        allTransactions = transactionDao.getAll()
        assertThat(allTransactions).doesNotContain(transaction)
    }

    @Test
    fun testUpdateTransactionShouldUpdateDatabase() {
        val transaction = Transaction(
            id = 1,
            amount = 40.0,
            date = Date(),
            description = "something to eat",
            expenseType = ExpenseType.OFFICE_EXPENSES
        )
        transactionDao.insert(transaction)
        var allTransactions = transactionDao.getAll()
        assertThat(allTransactions).contains(transaction)

        val transactionUpdated = Transaction(
            id = 1,
            amount = 50.0,
            date = Date(),
            description = "something to eat and drink",
            expenseType = ExpenseType.OFFICE_EXPENSES
        )
        transactionDao.update(transactionUpdated)
        allTransactions = transactionDao.getAll()
        assertThat(allTransactions).contains(transactionUpdated)
        assertThat(allTransactions).doesNotContain(transaction)
    }

    @Test
    fun testGetTotalAmountShouldReturnTotalAmount() {
        val transaction = Transaction(
            id = 1,
            amount = 40.0,
            date = Date(),
            description = "something to eat",
            expenseType = ExpenseType.OFFICE_EXPENSES
        )
        transactionDao.insert(transaction)


        val transaction2 = Transaction(
            id = 2,
            amount = 50.0,
            date = Date(),
            description = "something to eat and drink",
            expenseType = ExpenseType.OFFICE_EXPENSES
        )

        transactionDao.insert(transaction2)

        val totalAmount = transactionDao.getTotalAmount()
        assertThat(totalAmount).isEqualTo(90.0)
    }
}