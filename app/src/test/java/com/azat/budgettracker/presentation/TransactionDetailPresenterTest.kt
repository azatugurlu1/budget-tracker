package com.azat.budgettracker.presentation

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import com.azat.budgettracker.R
import com.azat.budgettracker.domain.entities.ExpenseType
import com.azat.budgettracker.domain.entities.Transaction
import com.azat.budgettracker.presentation.transactiondetail.TransactionDetailPresenter
import com.azat.budgettracker.presentation.transactiondetail.TransactionDetailViewModel
import com.azat.budgettracker.presentation.transactiondetail.TransactionDetailViewModelModelState
import com.azat.budgettracker.utils.*
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.atLeastOnce
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.util.*

class TransactionDetailPresenterTest {
    @Mock
    private lateinit var mutableLiveData: MutableLiveData<TransactionDetailViewModel>

    @Mock
    private lateinit var resourceProviderInterface: ResourceProviderInterface

    @Mock
    private lateinit var resources: Resources

    private lateinit var  transactionDetailPresenter: TransactionDetailPresenter
    private var injectionInterface = TestInjection
    private var schedulerProvider = TrampolineSchedulerProvider()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        transactionDetailPresenter = TransactionDetailPresenter(
            schedulerProvider,
            injectionInterface,
            mutableLiveData,
            resourceProviderInterface,
            Constants.NavigationArguments.CREATE,
            null
        )
        whenever(resourceProviderInterface.getResources()).thenReturn(resources)
        whenever(resourceProviderInterface.getResources().getString(R.string.expense_created)).thenReturn("Created")
        whenever(resourceProviderInterface.getResources().getString(R.string.expense_updated)).thenReturn("Updated")
        whenever(resourceProviderInterface.getResources().getString(R.string.expense_removed)).thenReturn("Removed")
        whenever(resourceProviderInterface.getResources().getString(R.string.update_expense)).thenReturn("Update expense")
        whenever(resourceProviderInterface.getResources().getString(R.string.create_expense)).thenReturn("Create expense")
    }

    @Test
    fun testOnCreateViewShouldSetViewModelStateLoaded() {
        val captor = ArgumentCaptor.forClass(
            TransactionDetailViewModel::class.java
        )
        transactionDetailPresenter.onCreateView()
        verify(mutableLiveData, atLeastOnce()).value = captor.capture()
        Assert.assertEquals(ViewModelState().loaded, captor.value.viewModelState)
    }

    @Test
    fun testOnAddTransactionShouldAddTransactionAndNavigateBackToMain() {
        val captor = ArgumentCaptor.forClass(
            TransactionDetailViewModel::class.java
        )
        val transaction = Transaction(null, 40.0, Date(), "Something", ExpenseType.OFFICE_EXPENSES)
        whenever(injectionInterface.transactionUseCases.addTransaction(transaction)).thenReturn(
            Single.just(1))
        transactionDetailPresenter.onAddTransaction(transaction)
        verify(mutableLiveData, atLeastOnce()).value = captor.capture()
        Assert.assertEquals(TransactionDetailViewModelModelState().navigateToMainTransaction, captor.value.viewModelState)
    }

    @Test
    fun testOnAddTransactionShouldSetViewModelStateError() {
        val captor = ArgumentCaptor.forClass(
            TransactionDetailViewModel::class.java
        )
        val transaction = Transaction(null, 40.0, null, "", null)
        whenever(injectionInterface.transactionUseCases.addTransaction(transaction)).thenReturn(
            Single.error(Error("error")))
        transactionDetailPresenter.onAddTransaction(transaction)
        verify(mutableLiveData, atLeastOnce()).value = captor.capture()
        Assert.assertEquals(TransactionDetailViewModelModelState().error, captor.value.viewModelState)
    }

    @Test
    fun testOnCreateViewEditModeShouldFetchTransaction() {
        val transactionDetailPresenter = TransactionDetailPresenter(
            schedulerProvider,
            injectionInterface,
            mutableLiveData,
            resourceProviderInterface,
            Constants.NavigationArguments.EDIT,
            1
        )
        val transaction = Transaction(1, 40.0, Date(), "Something", ExpenseType.OFFICE_EXPENSES)
        whenever(injectionInterface.transactionUseCases.getTransaction(1)).thenReturn(
            Single.just(transaction))
        val captor = ArgumentCaptor.forClass(
            TransactionDetailViewModel::class.java
        )
        transactionDetailPresenter.onCreateView()
        verify(injectionInterface.transactionUseCases, atLeastOnce()).getTransaction(any())
        verify(mutableLiveData, atLeastOnce()).value = captor.capture()
        Assert.assertEquals(ViewModelState().loaded, captor.value.viewModelState)
    }

    @Test
    fun testOnEditTransactionShouldUpdateAndNavigateBackToMain() {
        val transactionDetailPresenter = TransactionDetailPresenter(
            schedulerProvider,
            injectionInterface,
            mutableLiveData,
            resourceProviderInterface,
            Constants.NavigationArguments.EDIT,
            1
        )
        val transaction = Transaction(1, 40.0, Date(), "Something", ExpenseType.OFFICE_EXPENSES)
        whenever(injectionInterface.transactionUseCases.updateTransaction(transaction)).thenReturn(
            Single.just(1))
        val captor = ArgumentCaptor.forClass(
            TransactionDetailViewModel::class.java
        )
        transactionDetailPresenter.onEditTransaction(transaction)
        verify(injectionInterface.transactionUseCases, atLeastOnce()).updateTransaction(any())
        verify(mutableLiveData, atLeastOnce()).value = captor.capture()
        Assert.assertEquals(TransactionDetailViewModelModelState().navigateToMainTransaction, captor.value.viewModelState)
    }

    @Test
    fun testOnDeleteTransactionShouldDeleteAndNavigateBackToMain() {
        val transactionDetailPresenter = TransactionDetailPresenter(
            schedulerProvider,
            injectionInterface,
            mutableLiveData,
            resourceProviderInterface,
            Constants.NavigationArguments.EDIT,
            1
        )
        val transaction = Transaction(1, 40.0, Date(), "Something", ExpenseType.OFFICE_EXPENSES)
        whenever(injectionInterface.transactionUseCases.removeTransaction(transaction)).thenReturn(
            Single.just(1))
        val captor = ArgumentCaptor.forClass(
            TransactionDetailViewModel::class.java
        )
        transactionDetailPresenter.onDeleteTransaction(transaction)
        verify(injectionInterface.transactionUseCases, atLeastOnce()).removeTransaction(any())
        verify(mutableLiveData, atLeastOnce()).value = captor.capture()
        Assert.assertEquals(TransactionDetailViewModelModelState().navigateToMainTransaction, captor.value.viewModelState)
    }

    @Test
    fun testGetLiveDataShouldGiveSameObject() {
        Assert.assertEquals(mutableLiveData, transactionDetailPresenter.getLiveData())
    }
}