package com.azat.budgettracker.presentation

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import com.azat.budgettracker.R
import com.azat.budgettracker.presentation.main.MainFragmentPresenter
import com.azat.budgettracker.presentation.main.MainFragmentViewModel
import com.azat.budgettracker.utils.ResourceProviderInterface
import com.azat.budgettracker.utils.TestInjection
import com.azat.budgettracker.utils.TrampolineSchedulerProvider
import com.azat.budgettracker.utils.ViewModelState
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.atLeastOnce
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever


class MainFragmentPresenterTest {

    @Mock
    private lateinit var mutableLiveData: MutableLiveData<MainFragmentViewModel>

    @Mock
    private lateinit var resourceProviderInterface: ResourceProviderInterface

    @Mock
    private lateinit var resources: Resources

    private lateinit var  mainFragmentPresenter: MainFragmentPresenter
    private var injectionInterface = TestInjection
    private var schedulerProvider = TrampolineSchedulerProvider()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        whenever(injectionInterface.transactionUseCases.getTotalAmountOfAllTransactions()).thenReturn(Single.just(
            50.0))
        whenever(injectionInterface.transactionUseCases.getAllTransactions()).thenReturn(Single.just(
            emptyList()))
        whenever(resourceProviderInterface.getResources()).thenReturn(resources)
        whenever(resourceProviderInterface.getResources().getString(R.string.main_title)).thenReturn("Hi Alvar")
        mainFragmentPresenter = MainFragmentPresenter(schedulerProvider, injectionInterface, resourceProviderInterface, mutableLiveData)
    }

    @Test
    fun testOnCreateViewShouldFetchTransactions() {
        mainFragmentPresenter.onCreateView()
        verify(injectionInterface.transactionUseCases, atLeastOnce()).getAllTransactions()
    }

    @Test
    fun testOnCreateViewShouldFetchTotalAmountOfAllTransactions() {
        mainFragmentPresenter.onCreateView()
        verify(injectionInterface.transactionUseCases, atLeastOnce()).getTotalAmountOfAllTransactions()
    }

    @Test
    fun testOnCreateViewShouldFetchTotalAmountAndSetViewModelStateLoaded() {
        val captor = ArgumentCaptor.forClass(
            MainFragmentViewModel::class.java
        )
        mainFragmentPresenter.onCreateView()
        verify(mutableLiveData, atLeastOnce()).value = captor.capture()
        Assert.assertEquals(ViewModelState().loaded, captor.value.viewModelState)
    }

    @Test
    fun testGetLiveDataShouldGiveSameObject() {
        Assert.assertEquals(mutableLiveData, mainFragmentPresenter.getLiveData())
    }
}