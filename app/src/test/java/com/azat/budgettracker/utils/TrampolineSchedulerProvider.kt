package com.azat.budgettracker.utils

import io.reactivex.schedulers.Schedulers

class TrampolineSchedulerProvider : SchedulerProviderInterface {
    override fun computation() = Schedulers.trampoline()
    override fun ui() = Schedulers.trampoline()
    override fun io() = Schedulers.trampoline()
}
