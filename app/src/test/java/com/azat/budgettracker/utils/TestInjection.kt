package com.azat.budgettracker.utils

import com.azat.budgettracker.di.InjectionInterface
import com.azat.budgettracker.domain.usecases.TransactionUseCasesInterface
import org.mockito.kotlin.mock

object TestInjection: InjectionInterface {
    override val transactionUseCases: TransactionUseCasesInterface = mock()
}