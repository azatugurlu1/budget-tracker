package com.azat.budgettracker.utils
import io.reactivex.schedulers.TestScheduler

class TestSchedulerProvider(private val scheduler: TestScheduler): SchedulerProviderInterface {
    override fun computation() = scheduler
    override fun ui() = scheduler
    override fun io() = scheduler
}