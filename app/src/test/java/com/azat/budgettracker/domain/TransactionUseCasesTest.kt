package com.azat.budgettracker.domain

import com.azat.budgettracker.data.repositories.TransactionRepositoryInterface
import com.azat.budgettracker.domain.entities.ExpenseType
import com.azat.budgettracker.domain.entities.Transaction
import com.azat.budgettracker.domain.usecases.TransactionUseCases
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.verify
import org.mockito.kotlin.verifyNoInteractions
import java.util.*

class TransactionUseCasesTest {
    @Mock
    private lateinit var transactionRepositoryInterface: TransactionRepositoryInterface

    private lateinit var transactionUseCases: TransactionUseCases

    private val transaction = Transaction(1, 40.0, Date(), "Something", ExpenseType.OFFICE_EXPENSES)
    private val invalidTransaction = Transaction(null, 0.0, null, "Something", null)

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        transactionUseCases = TransactionUseCases(transactionRepositoryInterface)
    }

    @Test
    fun testGetAllTransactionsShouldCallRepository() {
        transactionUseCases.getAllTransactions()
        verify(transactionRepositoryInterface).getAll()
    }

    @Test
    fun testGetTransactionShouldCallRepository() {
        transactionUseCases.getTransaction(1)
        verify(transactionRepositoryInterface).getTransaction(1)
    }

    @Test
    fun testGetTotalAmountOfAllTransactionsShouldCallRepository() {
        transactionUseCases.getTotalAmountOfAllTransactions()
        verify(transactionRepositoryInterface).getTotalAmount()
    }

    @Test
    fun testAddTransactionShouldCallRepository() {
        transactionUseCases.addTransaction(transaction)
        verify(transactionRepositoryInterface).insert(transaction)
    }

    @Test
    fun testAddTransactionShouldNotInteractWithRepository() {
        transactionUseCases.addTransaction(invalidTransaction)
        verifyNoInteractions(transactionRepositoryInterface)
    }

    @Test
    fun testUpdateTransactionShouldCallRepository() {
        transactionUseCases.updateTransaction(transaction)
        verify(transactionRepositoryInterface).update(transaction)
    }

    @Test
    fun testUpdateTransactionShouldNotInteractWithRepository() {
        transactionUseCases.updateTransaction(invalidTransaction)
        verifyNoInteractions(transactionRepositoryInterface)
    }

    @Test
    fun testRemoveTransactionShouldCallRepository() {
        transactionUseCases.removeTransaction(transaction)
        verify(transactionRepositoryInterface).delete(transaction)
    }

    @Test
    fun testRemoveTransactionShouldNotInteractWithRepository() {
        transactionUseCases.removeTransaction(invalidTransaction)
        verifyNoInteractions(transactionRepositoryInterface)
    }


}