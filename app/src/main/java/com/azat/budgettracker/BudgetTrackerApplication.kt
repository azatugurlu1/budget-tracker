package com.azat.budgettracker

import android.app.Application
import androidx.room.Room
import com.azat.budgettracker.data.persistent.BudgetTrackerDatabase
import com.azat.budgettracker.di.Injection

open class BudgetTrackerApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        Injection.setDatabase(getDatabase())
    }

    private fun getDatabase():  BudgetTrackerDatabase {
        return Room.databaseBuilder(
            this,
            BudgetTrackerDatabase::class.java,
            "budget_tracker_database"
        ).build()
    }
}