package com.azat.budgettracker.utils

open class ViewModelState {
    val initial = "error"
    val loading = "loading"
    val loaded = "loaded"
    val error = "error"
}


