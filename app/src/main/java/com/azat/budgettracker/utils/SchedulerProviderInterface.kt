package com.azat.budgettracker.utils

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

interface SchedulerProviderInterface {
    fun io(): Scheduler
    fun computation(): Scheduler
    fun ui(): Scheduler
}