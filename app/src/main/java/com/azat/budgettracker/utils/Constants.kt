package com.azat.budgettracker.utils

object Constants {
    object NavigationArguments {
        val TYPE = "type"
        val TRANSACTION_ID = "transactionId"
        val CREATE = "create"
        val EDIT = "edit"
    }
}