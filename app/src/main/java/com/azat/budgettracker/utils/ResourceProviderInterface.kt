package com.azat.budgettracker.utils

import android.content.res.Resources

interface ResourceProviderInterface {
    fun getResources(): Resources
}