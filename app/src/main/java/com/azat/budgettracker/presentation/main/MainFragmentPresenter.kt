package com.azat.budgettracker.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.azat.budgettracker.R
import com.azat.budgettracker.di.InjectionInterface
import com.azat.budgettracker.domain.entities.Transaction
import com.azat.budgettracker.utils.ResourceProviderInterface
import com.azat.budgettracker.utils.SchedulerProviderInterface
import com.azat.budgettracker.utils.ViewModelState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

class MainFragmentPresenter(
    private val schedulerProvider: SchedulerProviderInterface,
    private val injectionInterface: InjectionInterface,
    private val resourceProviderInterface: ResourceProviderInterface,
    val mutableLiveData: MutableLiveData<MainFragmentViewModel>
    ): MainFragmentPresenterInterface {

    private val viewModel = MainFragmentViewModel()
    private val disposable = CompositeDisposable()

    override fun onCreateView() {
        createInitialViewModel()
        setLoading()
        getTotalAmount()
        getAllTransactions()
    }

    private fun getTotalAmount() {
        disposable.add(
            injectionInterface.transactionUseCases.getTotalAmountOfAllTransactions()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : DisposableSingleObserver<Double>() {
                    override fun onSuccess(totalAmount: Double) {
                        viewModel.viewModelState = ViewModelState().loaded
                        viewModel.totalAmount = totalAmount
                        mutableLiveData.value = viewModel
                    }

                    override fun onError(error: Throwable) {
                        setError(error.localizedMessage)
                    }
                })
        )
    }

    private fun getAllTransactions() {
        disposable.add(
            injectionInterface.transactionUseCases.getAllTransactions()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : DisposableSingleObserver<List<Transaction>>() {
                    override fun onSuccess(transactions: List<Transaction>) {
                        viewModel.viewModelState = ViewModelState().loaded
                        viewModel.transactions = transactions
                        mutableLiveData.value = viewModel
                    }

                    override fun onError(error: Throwable) {
                        setError(error.localizedMessage)
                    }
                })
        )
    }

    private fun createInitialViewModel() {
        viewModel.viewModelState = ViewModelState().initial
        viewModel.userName = resourceProviderInterface.getResources().getString(R.string.main_title)
    }

    private fun setLoading() {
        viewModel.viewModelState = ViewModelState().loading
        mutableLiveData.value = viewModel
    }

    private fun setError(message: String?) {
        viewModel.viewModelState = ViewModelState().error
        viewModel.errorMessage = message ?: resourceProviderInterface.getResources().getString(R.string.error_occurred)
    }

    override fun getLiveData(): LiveData<MainFragmentViewModel> {
        return mutableLiveData
    }

    override fun onDestroyView() {
        disposable.clear()
    }
}