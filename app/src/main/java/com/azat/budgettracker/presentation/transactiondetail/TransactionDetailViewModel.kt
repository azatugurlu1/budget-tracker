package com.azat.budgettracker.presentation.transactiondetail

import android.view.View
import com.azat.budgettracker.domain.entities.ExpenseType
import com.azat.budgettracker.domain.entities.Transaction
import com.azat.budgettracker.utils.ViewModelState

class TransactionDetailViewModel {
    var viewModelState = TransactionDetailViewModelModelState().initial
    var transactionTypes: Array<ExpenseType> = emptyArray()
    var transaction: Transaction = Transaction(null, 0.0, null, "", ExpenseType.OFFICE_EXPENSES)
    var title = ""
    var message = ""
    var errorMessage = ""
    var createButtonVisibility = View.VISIBLE
    var editButtonVisibility = View.GONE
    var deleteButtonVisibility = View.GONE
}

class TransactionDetailViewModelModelState: ViewModelState() {
    val navigateToMainTransaction = "navigateToMainTransaction"
}