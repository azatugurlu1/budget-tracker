package com.azat.budgettracker.presentation

interface RecyclerviewOnClickListener {
    fun recyclerviewClick(position: Int)
}