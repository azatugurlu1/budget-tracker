package com.azat.budgettracker.presentation.main

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.azat.budgettracker.R
import com.azat.budgettracker.databinding.TransactionViewHolderBinding
import com.azat.budgettracker.domain.entities.Transaction
import com.azat.budgettracker.presentation.RecyclerviewOnClickListener
import java.text.SimpleDateFormat

class TransactionsAdapter(
    private var transactions: List<Transaction>,
    private var context: Context,
    private var clickListener: RecyclerviewOnClickListener):
    RecyclerView.Adapter<TransactionsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val transactionViewHolderBinding = TransactionViewHolderBinding.inflate(
            LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(transactionViewHolderBinding)
    }

    fun updateTransactions(transactions: List<Transaction>) {
        this.transactions = transactions
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            with(transactions[position]) {
                transactionViewHolderBinding.textViewTransactionAmount.text =
                    transactionViewHolderBinding.root.resources.getString(R.string.euro_text_negative, amount)
                transactionViewHolderBinding.textViewTransactionDescription.text = description
                val formattedDate = SimpleDateFormat("dd MMMM yyyy").format(date!!)
                transactionViewHolderBinding.textViewTransactionDate.text = formattedDate
                transactionViewHolderBinding.textViewTransactionTag.text = expenseType?.let {
                    context.getString(it.getTextId())
                }

                holder.itemView.setOnClickListener {
                    clickListener.recyclerviewClick(position)
                }
            }
        }
    }

    override fun getItemCount() = transactions.size

    inner class ViewHolder(val transactionViewHolderBinding: TransactionViewHolderBinding):
        RecyclerView.ViewHolder(transactionViewHolderBinding.root)
}