package com.azat.budgettracker.presentation.transactiondetail

import androidx.lifecycle.LiveData
import com.azat.budgettracker.domain.entities.Transaction
import java.util.*

interface TransactionDetailPresenterInterface {
    fun onCreateView()
    fun getLiveData(): LiveData<TransactionDetailViewModel>
    fun onAddTransaction(transaction: Transaction)
    fun onEditTransaction(transaction: Transaction)
    fun onDeleteTransaction(transaction: Transaction)
    fun onDestroyView()
}