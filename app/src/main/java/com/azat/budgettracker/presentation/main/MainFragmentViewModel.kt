package com.azat.budgettracker.presentation.main

import com.azat.budgettracker.domain.entities.Transaction
import com.azat.budgettracker.utils.ViewModelState

class MainFragmentViewModel {
    var viewModelState = ViewModelState().initial
    var transactions: List<Transaction> = emptyList()
    var totalAmount = 0.0
    var userName = ""
    var errorMessage = ""
}

class MainFragmentViewModelState: ViewModelState() {
    val navigateToEditTransaction = "navigateToEditTransaction"
}