package com.azat.budgettracker.presentation.main

import androidx.lifecycle.LiveData
import com.azat.budgettracker.domain.entities.Transaction

interface MainFragmentPresenterInterface {
    fun onCreateView()
    fun getLiveData(): LiveData<MainFragmentViewModel>
    fun onDestroyView()
}