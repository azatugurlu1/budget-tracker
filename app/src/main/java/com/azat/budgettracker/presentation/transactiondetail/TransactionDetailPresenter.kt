package com.azat.budgettracker.presentation.transactiondetail

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.azat.budgettracker.R
import com.azat.budgettracker.di.InjectionInterface
import com.azat.budgettracker.domain.entities.ExpenseType
import com.azat.budgettracker.domain.entities.Transaction
import com.azat.budgettracker.utils.Constants
import com.azat.budgettracker.utils.ResourceProviderInterface
import com.azat.budgettracker.utils.SchedulerProviderInterface
import com.azat.budgettracker.utils.ViewModelState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.util.*

class TransactionDetailPresenter(
    private val schedulerProvider: SchedulerProviderInterface,
    private val injectionInterface: InjectionInterface,
    val mutableLiveData: MutableLiveData<TransactionDetailViewModel>,
    private val resourceProviderInterface: ResourceProviderInterface,
    private val transactionType: String,
    private val transactionId: Int?
): TransactionDetailPresenterInterface {

    private val viewModel = TransactionDetailViewModel()
    private val disposable = CompositeDisposable()


    override fun onCreateView() {
        createInitialViewModel()
        setLoading()
        if (transactionType == Constants.NavigationArguments.EDIT) {
            if (transactionId != null) {
                getTransaction(transactionId)
            }
        } else {
            setupCreateTransaction()
        }

    }

    private fun setupCreateTransaction() {
        viewModel.viewModelState = ViewModelState().loaded
        viewModel.title = resourceProviderInterface.getResources().getString(R.string.create_expense)
        viewModel.createButtonVisibility = View.VISIBLE
        viewModel.editButtonVisibility = View.GONE
        viewModel.deleteButtonVisibility = View.GONE
        mutableLiveData.value = viewModel
    }

    override fun getLiveData(): LiveData<TransactionDetailViewModel> {
        return mutableLiveData
    }

    private fun createInitialViewModel() {
        viewModel.viewModelState = ViewModelState().initial
        viewModel.transactionTypes = ExpenseType.values()
    }

    private fun setLoading() {
        viewModel.viewModelState = ViewModelState().loading
        mutableLiveData.value = viewModel
    }

    private fun setError(message: String?) {
        viewModel.viewModelState = ViewModelState().error
        viewModel.errorMessage = message ?: resourceProviderInterface.getResources().getString(R.string.error_occurred)
        mutableLiveData.value = viewModel
    }

    private fun getTransaction(transactionId: Int) {
        disposable.add(
            injectionInterface.transactionUseCases.getTransaction(transactionId)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : DisposableSingleObserver<Transaction>() {
                    override fun onSuccess(transaction: Transaction) {
                        viewModel.viewModelState = ViewModelState().loaded
                        viewModel.title = resourceProviderInterface.getResources().getString(R.string.update_expense)
                        viewModel.transaction = transaction
                        viewModel.createButtonVisibility = View.GONE
                        viewModel.editButtonVisibility = View.VISIBLE
                        viewModel.deleteButtonVisibility = View.VISIBLE
                        mutableLiveData.value = viewModel
                    }

                    override fun onError(error: Throwable) {
                        setError(error.localizedMessage)
                    }
                })
        )
    }

    override fun onAddTransaction(transaction: Transaction) {
        disposable.add(
            injectionInterface.transactionUseCases.addTransaction(transaction)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : DisposableSingleObserver<Long>() {
                    override fun onSuccess(effectedLines: Long) {
                        viewModel.viewModelState = TransactionDetailViewModelModelState().navigateToMainTransaction
                        viewModel.message = resourceProviderInterface.getResources().getString(R.string.expense_created)
                        mutableLiveData.value = viewModel
                    }

                    override fun onError(error: Throwable) {
                        setError(error.localizedMessage)
                    }
                })
        )
    }

    override fun onEditTransaction(transaction: Transaction) {
        disposable.add(
            injectionInterface.transactionUseCases.updateTransaction(transaction)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : DisposableSingleObserver<Int>() {
                    override fun onSuccess(status: Int) {
                        viewModel.viewModelState = TransactionDetailViewModelModelState().navigateToMainTransaction
                        viewModel.message = resourceProviderInterface.getResources().getString(R.string.expense_updated)
                        mutableLiveData.value = viewModel
                    }

                    override fun onError(error: Throwable) {
                        setError(error.localizedMessage)
                    }
                })
        )
    }

    override fun onDeleteTransaction(transaction: Transaction) {
        disposable.add(
            injectionInterface.transactionUseCases.removeTransaction(transaction)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribeWith(object : DisposableSingleObserver<Int>() {
                    override fun onSuccess(status: Int) {
                        viewModel.viewModelState = TransactionDetailViewModelModelState().navigateToMainTransaction
                        viewModel.message = resourceProviderInterface.getResources().getString(R.string.expense_removed)
                        mutableLiveData.value = viewModel
                    }

                    override fun onError(error: Throwable) {
                        setError(error.localizedMessage)
                    }
                })
        )
    }

    override fun onDestroyView() {
        disposable.clear()
    }
}