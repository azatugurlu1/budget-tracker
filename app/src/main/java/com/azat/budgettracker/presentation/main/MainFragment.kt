package com.azat.budgettracker.presentation.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.azat.budgettracker.R
import com.azat.budgettracker.databinding.FragmentMainBinding
import com.azat.budgettracker.di.Injection
import com.azat.budgettracker.domain.entities.ExpenseType
import com.azat.budgettracker.domain.entities.Transaction
import com.azat.budgettracker.presentation.RecyclerviewOnClickListener
import com.azat.budgettracker.utils.*
import com.google.android.material.snackbar.Snackbar
import java.util.*

class MainFragment: Fragment(), RecyclerviewOnClickListener, ResourceProviderInterface {

    private var fragmentMainBinding: FragmentMainBinding? = null
    private val binding get() = fragmentMainBinding!!
    private lateinit var mainFragmentPresenter: MainFragmentPresenterInterface
    private lateinit var transactionsAdapter: TransactionsAdapter
    private var transactions: List<Transaction> = emptyList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentMainBinding = FragmentMainBinding.inflate(inflater, container, false)
        setupRecyclerView()
        mainFragmentPresenter = MainFragmentPresenter(SchedulerProvider(), Injection, this, MutableLiveData())
        mainFragmentPresenter.getLiveData().observe(viewLifecycleOwner, this::render)
        mainFragmentPresenter.onCreateView()
        binding.buttonAddExpense.setOnClickListener {
            val bundle = bundleOf(
                Constants.NavigationArguments.TYPE to Constants.NavigationArguments.CREATE)
            findNavController().navigate(R.id.action_MainFragment_to_DetailFragment, bundle)
        }
        return binding.root
    }

    private fun render(viewModel: MainFragmentViewModel) {
        when(viewModel.viewModelState) {
            ViewModelState().loaded -> {
                binding.textViewUser.text = viewModel.userName
                val amount = viewModel.totalAmount.round(2)
                binding.textViewTotalAmount.text = getString(R.string.euro_text, amount)
                transactions = viewModel.transactions
                transactionsAdapter.updateTransactions(transactions)
            }
            ViewModelState().error -> {
                Snackbar.make(binding.root, viewModel.errorMessage, 2000).show()
            }
            else -> {

            }
        }
    }

    private fun setupRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)
        binding.recyclerViewAllExpenses.layoutManager = layoutManager
        binding.recyclerViewAllExpenses.addItemDecoration(
            DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        )
        transactionsAdapter = TransactionsAdapter(transactions, requireContext(), this)
        binding.recyclerViewAllExpenses.adapter = transactionsAdapter
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentMainBinding = null
    }

    override fun recyclerviewClick(position: Int) {
        val bundle = bundleOf(
            Constants.NavigationArguments.TYPE to Constants.NavigationArguments.EDIT,
            Constants.NavigationArguments.TRANSACTION_ID to transactions[position].id)
        findNavController().navigate(R.id.action_MainFragment_to_DetailFragment, bundle)
    }
}