package com.azat.budgettracker.presentation.transactiondetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import com.azat.budgettracker.R
import com.azat.budgettracker.databinding.FragmentTransactionDetailBinding
import com.azat.budgettracker.di.Injection
import com.azat.budgettracker.domain.entities.ExpenseType
import com.azat.budgettracker.presentation.main.MainFragmentPresenter
import com.azat.budgettracker.presentation.main.MainFragmentViewModel
import com.azat.budgettracker.utils.*
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*

class TransactionDetailFragment : Fragment(), ResourceProviderInterface, AdapterView.OnItemSelectedListener {

    private var fragmentTransactionDetailBinding: FragmentTransactionDetailBinding? = null
    private val binding get() = fragmentTransactionDetailBinding!!
    private lateinit var transactionDetailPresenter: TransactionDetailPresenterInterface
    private lateinit var expensesAdapter: ArrayAdapter<*>
    private lateinit var viewModel: TransactionDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentTransactionDetailBinding = FragmentTransactionDetailBinding.inflate(inflater, container, false)

        val type = arguments?.getString(Constants.NavigationArguments.TYPE)
        val transactionId = arguments?.getInt(Constants.NavigationArguments.TRANSACTION_ID)
        var transactionType = Constants.NavigationArguments.CREATE
        if (Constants.NavigationArguments.EDIT == type) {
            transactionType = Constants.NavigationArguments.EDIT
        }

        transactionDetailPresenter = TransactionDetailPresenter(
            SchedulerProvider(),
            Injection,
            MutableLiveData(),
            this,
            transactionType,
            transactionId
        )
        transactionDetailPresenter.getLiveData().observe(viewLifecycleOwner, this::render)
        transactionDetailPresenter.onCreateView()
        setUpListeners()
        return binding.root

    }

    private fun render(viewModel: TransactionDetailViewModel) {
        this.viewModel = viewModel
        when(viewModel.viewModelState) {
            TransactionDetailViewModelModelState().loaded -> {
                updateViews(viewModel)
            }
            TransactionDetailViewModelModelState().error -> {
                Snackbar.make(binding.root, viewModel.errorMessage, 2000).show()
            }
            TransactionDetailViewModelModelState().navigateToMainTransaction -> {
                Snackbar.make(binding.root, viewModel.message, 2000).show()
                findNavController().navigate(R.id.action_DetailFragment_to_MainFragment)
            }
            else -> {

            }
        }
    }

    private fun setUpListeners() {
        binding.spinnerTransactionType.onItemSelectedListener = this
        binding.buttonCreate.setOnClickListener {
            viewModel.transaction.amount = binding.editTextAmount.text.toString().toDouble().round(2)
            viewModel.transaction.description = binding.editTextDescription.text.toString()
            transactionDetailPresenter.onAddTransaction(viewModel.transaction)
        }

        binding.buttonEdit.setOnClickListener {
            viewModel.transaction.amount = binding.editTextAmount.text.toString().toDouble().round(2)
            viewModel.transaction.description = binding.editTextDescription.text.toString()
            transactionDetailPresenter.onEditTransaction(viewModel.transaction)
        }

        binding.buttonDelete.setOnClickListener {
            transactionDetailPresenter.onDeleteTransaction(viewModel.transaction)
        }

        binding.buttonSelectDate.setOnClickListener {
            val datePicker =
                MaterialDatePicker.Builder.datePicker()
                    .setTitleText("Select date")
                    .setSelection(MaterialDatePicker.todayInUtcMilliseconds())
                    .build()

            datePicker.addOnPositiveButtonClickListener {
                if (datePicker.selection != null) {
                    val date = Date(datePicker.selection!!)
                    viewModel.transaction.date = date
                    binding.textViewDate.text = getFormattedDate(date)
                }
            }
            datePicker.show(requireActivity().supportFragmentManager, "Date Picker")

        }
    }

    private fun updateViews(viewModel: TransactionDetailViewModel) {
        binding.textViewTitle.text = viewModel.title
        binding.buttonCreate.visibility = viewModel.createButtonVisibility
        binding.buttonEdit.visibility = viewModel.editButtonVisibility
        binding.buttonDelete.visibility = viewModel.deleteButtonVisibility

        expensesAdapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.expense_type_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spinnerTransactionType.adapter = adapter
        }

        viewModel.transaction.description.let { binding.editTextDescription.setText(it) }
        viewModel.transaction.amount.let { binding.editTextAmount.setText(it.toString()) }
        viewModel.transaction.date?.let {
            val formattedDate = getFormattedDate(it)
            binding.textViewDate.text = formattedDate
        }
        viewModel.transaction.expenseType?.let {
            binding.spinnerTransactionType.setSelection(
                (expensesAdapter as ArrayAdapter<CharSequence>).getPosition(getString(it.getTextId()))
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fragmentTransactionDetailBinding = null
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (parent != null) {
            val itemAtPosition = parent.getItemAtPosition(position)
            viewModel.transaction.expenseType = ExpenseType.from(itemAtPosition.toString())
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {}

    private fun getFormattedDate(date: Date): String {
        return SimpleDateFormat("dd MMMM yyyy").format(date)
    }
}