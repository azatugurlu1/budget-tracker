package com.azat.budgettracker.data.persistent

import androidx.room.*
import com.azat.budgettracker.domain.entities.Transaction


@Dao
interface TransactionDao {
    @Query("SELECT * from transactions")
    fun getAll(): List<Transaction>

    @Query("SELECT * from transactions where id = :id")
    fun getTransaction(id: Int): Transaction

    @Query("SELECT SUM(amount) from transactions")
    fun getTotalAmount(): Double

    @Insert
    fun insert(transaction: Transaction): Long

    @Delete
    fun delete(transaction: Transaction): Int

    @Update
    fun update(transaction: Transaction): Int
}