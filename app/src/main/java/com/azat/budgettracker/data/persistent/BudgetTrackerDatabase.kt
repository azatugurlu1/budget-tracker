package com.azat.budgettracker.data.persistent

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.azat.budgettracker.domain.entities.Transaction

@Database(entities = [Transaction::class], version = 1)
@TypeConverters(Converters::class)
abstract class BudgetTrackerDatabase: RoomDatabase() {
    abstract fun transactionDao() : TransactionDao
}