package com.azat.budgettracker.data.persistent

import androidx.room.TypeConverter
import com.azat.budgettracker.domain.entities.ExpenseType
import java.util.*

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun toExpenseType(value: String) = enumValueOf<ExpenseType>(value)

    @TypeConverter
    fun fromExpenseType(value: ExpenseType) = value.name
}