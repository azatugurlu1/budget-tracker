package com.azat.budgettracker.data.repositories
import com.azat.budgettracker.data.persistent.TransactionDao
import com.azat.budgettracker.domain.entities.Transaction
import io.reactivex.Single

class TransactionRepository(private val transactionDao: TransactionDao): TransactionRepositoryInterface {
    override fun getAll(): Single<List<Transaction>> {
        return Single.fromCallable { transactionDao.getAll() }
    }

    override fun getTransaction(id: Int): Single<Transaction> {
        return  Single.fromCallable { transactionDao.getTransaction(id) }
    }

    override fun getTotalAmount(): Single<Double> {
        return Single.fromCallable { transactionDao.getTotalAmount() }
    }
    override fun insert(transaction: Transaction): Single<Long> {
        return Single.fromCallable { transactionDao.insert(transaction) }
    }

    override fun update(transaction: Transaction): Single<Int> {
        return Single.fromCallable { transactionDao.update(transaction) }
    }

    override fun delete(transaction: Transaction): Single<Int> {
        return Single.fromCallable { transactionDao.delete(transaction) }
    }
}