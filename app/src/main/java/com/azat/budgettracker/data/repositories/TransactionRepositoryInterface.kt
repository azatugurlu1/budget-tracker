package com.azat.budgettracker.data.repositories

import com.azat.budgettracker.domain.entities.Transaction
import io.reactivex.Single


interface TransactionRepositoryInterface {
    fun getAll(): Single<List<Transaction>>
    fun getTransaction(id: Int): Single<Transaction>
    fun getTotalAmount(): Single<Double>
    fun insert(transaction: Transaction): Single<Long>
    fun update(transaction: Transaction): Single<Int>
    fun delete(transaction: Transaction): Single<Int>
}