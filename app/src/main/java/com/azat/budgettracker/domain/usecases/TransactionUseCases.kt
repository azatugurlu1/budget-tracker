package com.azat.budgettracker.domain.usecases

import com.azat.budgettracker.data.repositories.TransactionRepositoryInterface
import com.azat.budgettracker.domain.entities.Transaction
import io.reactivex.Single
import java.lang.Error
import java.text.SimpleDateFormat
import java.util.*

class TransactionUseCases(private val transactionRepository: TransactionRepositoryInterface) :
    TransactionUseCasesInterface {
    override fun getAllTransactions(): Single<List<Transaction>> {
        return transactionRepository.getAll()
    }

    override fun getTransaction(id: Int): Single<Transaction> {
        return transactionRepository.getTransaction(id)
    }

    override fun getTotalAmountOfAllTransactions(): Single<Double> {
        return transactionRepository.getTotalAmount()
    }

    override fun addTransaction(transaction: Transaction): Single<Long> {
        return if (isTransactionValid(transaction)) {
            transactionRepository.insert(transaction)
        } else {
            Single.error(Error("Invalid Transaction"))
        }
    }

    override fun updateTransaction(transaction: Transaction): Single<Int> {
        return if (isTransactionValid(transaction)) {
            transactionRepository.update(transaction)
        } else {
            Single.error(Error("Invalid Transaction"))
        }
    }

    override fun removeTransaction(transaction: Transaction): Single<Int> {
        return if (isTransactionValid(transaction)) {
            transactionRepository.delete(transaction)
        } else {
            Single.error(Error("Invalid Transaction"))
        }

    }

    private fun isTransactionValid(transaction: Transaction): Boolean {
        if ((transaction.amount < 0 || transaction.amount == 0.0) ||
            transaction.description.isEmpty() ||
            transaction.date == null ||
            transaction.expenseType == null
        ) {
            return false
        }

        return true
    }
}
