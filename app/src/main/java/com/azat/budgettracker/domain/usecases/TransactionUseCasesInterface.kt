package com.azat.budgettracker.domain.usecases

import com.azat.budgettracker.domain.entities.Transaction
import io.reactivex.Single

interface TransactionUseCasesInterface {
    fun getAllTransactions(): Single<List<Transaction>>
    fun getTransaction(id: Int): Single<Transaction>
    fun getTotalAmountOfAllTransactions(): Single<Double>
    fun addTransaction(transaction: Transaction): Single<Long>
    fun updateTransaction(transaction: Transaction): Single<Int>
    fun removeTransaction(transaction: Transaction): Single<Int>
}