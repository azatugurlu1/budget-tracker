package com.azat.budgettracker.domain.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "transactions")
data class Transaction(
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
    var amount: Double,
    var date: Date? = null,
    var description: String,
    var expenseType: ExpenseType? = null
) : Serializable