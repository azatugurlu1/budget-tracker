package com.azat.budgettracker.domain.entities

import com.azat.budgettracker.R

enum class ExpenseType(val value: String) {
    OFFICE_EXPENSES("Office Expenses") {
        override fun getTextId(): Int = R.string.office_expenses
    },
    FOOD_AND_DRINKS("Food and Drinks") {
        override fun getTextId(): Int = R.string.food_and_drinks
    },
    SOFTWARE("Software") {
        override fun getTextId(): Int = R.string.software
    },
    SHOPPING("Shopping") {
        override fun getTextId(): Int = R.string.shopping
    },
    TRANSPORT("Transport") {
        override fun getTextId(): Int = R.string.transport
    },
    HEALTH_AND_BEAUTY("Health and Beauty") {
        override fun getTextId(): Int = R.string.health_and_beauty
    },
    OTHER("Other") {
        override fun getTextId(): Int = R.string.other
    };

    abstract fun getTextId(): Int

    companion object {
        fun from(findValue: String): ExpenseType = values().first { it.value == findValue }
    }
}