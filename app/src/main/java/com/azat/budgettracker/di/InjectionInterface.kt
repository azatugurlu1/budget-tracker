package com.azat.budgettracker.di

import com.azat.budgettracker.domain.usecases.TransactionUseCasesInterface

interface InjectionInterface {
    val transactionUseCases: TransactionUseCasesInterface
}