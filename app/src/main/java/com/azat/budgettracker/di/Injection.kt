package com.azat.budgettracker.di

import com.azat.budgettracker.data.persistent.BudgetTrackerDatabase
import com.azat.budgettracker.data.repositories.TransactionRepository
import com.azat.budgettracker.data.repositories.TransactionRepositoryInterface
import com.azat.budgettracker.domain.usecases.TransactionUseCases
import com.azat.budgettracker.domain.usecases.TransactionUseCasesInterface

object Injection: InjectionInterface {
    override val transactionUseCases: TransactionUseCasesInterface by lazy {
        TransactionUseCases(transactionRepository)
    }

    private lateinit var database: BudgetTrackerDatabase

    fun setDatabase(database: BudgetTrackerDatabase) {
        this.database = database
    }

    private val transactionRepository: TransactionRepositoryInterface
        get() = TransactionRepository(database.transactionDao())
}